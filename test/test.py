print("Checking html syntax")
from tidylib import tidy_document
with open('./public/index.html') as f:
    file = f.read()
_, errors = tidy_document(file, options={'show_warnings':0})
_, warnings = tidy_document(file, options={'show_warnings':1})
print("Number of warnings:", len(warnings), "errors:", len(errors))
print(warnings)
assert(len(errors) == 0)
